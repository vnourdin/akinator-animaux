# Projet Scala DUT #

### Akinator simplifié ###

Ce projet est une version simplifiée d'[Akinator](http://fr.akinator.com/) qui ne devine que des animaux. Lorsque le jeu ne connait pas la réponse, il demande quelques précisions au joueur afin d'apprendre.